package br.com.itau.investimento.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class SimulacaoServiceTest {

//	@MockBean
//	CatalogoService catalogoService;
	
//	@MockBean
//	AplicacaoRepository aplicacaoRepository;
	
//	@Autowired
	SimulacaoService simulacaoService;
	
	Aplicacao aplicacao;
	
	Optional<Produto> produtoOptional;
	
	@Before
	public void inicializar() {
		aplicacao = new Aplicacao();
		aplicacao.setIdProduto(1);
		aplicacao.setMeses(12);
		aplicacao.setEmail("teste@teste.com");
		aplicacao.setValor(1000);
		
		Produto produto = new Produto();
		produto.setId(1);
		produto.setNome("Poupança");
		produto.setRendimento(0.005);
		
		produtoOptional = Optional.of(produto);
		
		simulacaoService = new SimulacaoService();
		simulacaoService.catalogoService = Mockito.mock(CatalogoService.class);
		simulacaoService.aplicacaoRepository = Mockito.mock(AplicacaoRepository.class);
	}
	
	@Test
	public void testarProdutoInexistente() {
		Mockito.when(simulacaoService.catalogoService.obterProdutoPorId(aplicacao.getIdProduto())).thenReturn(Optional.empty());
		
		List<Simulacao> simulacoes = simulacaoService.calcular(aplicacao);
		
		assertNull(simulacoes);
	}
	
	@Test
	public void testarCalculo() {
		Mockito.when(simulacaoService.catalogoService.obterProdutoPorId(aplicacao.getIdProduto())).thenReturn(produtoOptional);
		
		List<Simulacao> simulacoes = simulacaoService.calcular(aplicacao);
		
		assertEquals(simulacoes.size(), 12);
	}
}
